package test.java.com.libertymutual.student.Jon.programs.example01.shapes;

import org.junit.Test;
import java.awt.Color;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class CircleTest {

    @Test
    public void testGetArea() {
        Circle circle = new Circle(10, Color.red);
        BigDecimal area = circle.getArea();
        BigDecimal expectedAnswer = new BigDecimal(314);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
