package test.java.com.libertymutual.student.Jon.programs.example01.shapes;

import org.junit.Test;
import java.awt.Color;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void testGetArea() {
        Rectangle rectangle = new Rectangle(10,Color.RED);
        BigDecimal area = rectangle.getArea();
        BigDecimal expectedAnswer = new BigDecimal(100);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
