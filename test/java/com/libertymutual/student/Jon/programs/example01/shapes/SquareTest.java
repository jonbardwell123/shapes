package test.java.com.libertymutual.student.Jon.programs.example1.shapes;

import org.junit.Test;
import java.awt.Color;
import java.math.BigDecimal;
import static org.junit.Assert.assertEquals;

public class SquareTest {

    @Test
    public void testGetArea() {
        Square square = new Square(10,Color.RED);
        BigDecimal area = square.getArea();
        BigDecimal expectedAnswer = new BigDecimal(100);
        assertEquals("Verify that the area is correct", expectedAnswer, area);
    }

}
